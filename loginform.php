<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles/loginformstyle.css">
</head>
<body>
    <ul class="nav-menu">
        <li><a href="homepage.html">Home</a></li>
        <li><a href="registrationform.php">Sign up</a></li>
    </ul>

    <div class="login-form">
        <form action="includes/login.inc.php" method="post">
            E-mail/Username: 
            <input type="text" name="emailuname" placeholder="Email...">
            <br>
            Password: 
            <input type="password" name="pass" placeholder="Password...">
            <br>
            <button type="submit" name="login-submit">Login</button>
        </form>
    </div>
</body>
</html>