<?php
    if(isset($_POST['signupButton'])){
        require 'dbConn.inc.php';

        $username = $_POST['name'];
        $userEmail = $_POST['email'];
        $userPassword = $_POST['pass'];
        $passRepeat = $_POST['repeatPass'];

        if(empty($username) || empty($userEmail) || empty($userPassword) || empty($passRepeat)){
            header("Location: ../registrationform.php?error=emptyfields&name".$username."&email=".$userEmail);
            exit();
        }
        else if(!filter_var($userEmail ,FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)){
            header("Location: ../registrationform.php?error=invalidemailandusername");
            exit();
        }
        else if (!filter_var($userEmail ,FILTER_VALIDATE_EMAIL)){
            header("Location: ../registrationform.php?error=invalidemail&name".$username);
            exit();
        }
        else if(!preg_match("/^[a-zA-Z0-9]*$/", $username)){
            header("Location: ../registrationform.php?error=invalidusername&email=".$userEmail);
            exit();
        }
        
        else if($userPassword !== $passRepeat){
            header("Location: ../registrationform.php?error=passdontmatch&name=".$username."&email=".$userEmail);
            exit();
        }
        else{
            $sql = "SELECT email FROM users WHERE email=?";
            $stmt = mysqli_stmt_init($dbConn);
            if(!mysqli_stmt_prepare($stmt, $sql)){
                header("Location: ../registrationform.php?error=sqlerror");
                exit();
            }
            else{
                mysqli_stmt_bind_param($stmt, "s", $username);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_store_result($stmt);

                $result = mysqli_stmt_num_rows($stmt);
                
                if($result > 0){
                    header("Location: ../registrationform.php?error=usertaken");
                    exit();
                }
                else{
                    $sql = "INSERT INTO users (userName, email, psw) VALUES (?, ?, ?)";
                    $stmt = mysqli_stmt_init($dbConn);
                    if(!mysqli_stmt_prepare($stmt, $sql)){
                        header("Location: ../registrationform.php?error=sqlerror");
                        exit();
                    }
                    else{
                        $hashedPass = password_hash($userPassword, PASSWORD_DEFAULT);

                        mysqli_stmt_bind_param($stmt, "sss", $username, $userEmail, $hashedPass);
                        mysqli_stmt_execute($stmt);
                        header("Location: ../registrationform.php?success_signup=success");
                        exit();
                    }
                }
            }
        }
        mysqli_stmt_close($stmt);
        mysqli_close($dbConn);
    }
    else{
        header("Location: ../registrationform.php");
        exit();
    }
?>