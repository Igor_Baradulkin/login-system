<?php
    $servername = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbName = "loginsysdb";

    $dbConn = mysqli_connect($servername, $dbUsername, $dbPassword, $dbName);

    if(!$dbConn){
        die("Connection failed" . mysqli_connect_error());
    } 
?>