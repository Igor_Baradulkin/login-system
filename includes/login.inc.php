<?php
    if(isset($_POST['login-submit'])){
        require 'dbConn.inc.php';

        $emailuname = $_POST['emailuname'];
        $pass = $_POST['pass'];

        if(empty($emailuname) || empty($pass)){
            header("Location: ../loginform.php?error=emptyfield");
            exit();
        }
        else{
            $sql = "SELECT * FROM users WHERE userName=? OR email=?;";
            $stmt = mysqli_stmt_init($dbConn);
            if(!mysqli_stmt_prepare($stmt, $sql)){
                header("Location: ../loginform.php?error=sqlerror");
                exit();
            }
            else{
                mysqli_stmt_bind_param($stmt, "ss", $emailuname, $emailuname);
                mysqli_stmt_execute($stmt);
                $result = mysqli_stmt_get_result($stmt);

                if($row = mysqli_fetch_assoc($result)){
                    $pwdCheck = password_verify($pass, $row['psw']);
                    if($pwdCheck == false){
                        header("Location: ../loginform.php?error=wrongpass");
                        exit();
                    }
                    else if($pwdCheck == true){
                        header("Location: ../loginform.php?message=success");
                        exit();
                    }
                    else{
                        header("Location: ../loginform.php?error=invalidinfo");
                        exit();
                    } 
                }
            }
        }
    }
    else{
        header("Location: ../loginform.php");
        exit();
    }
?>